import lt.itakademija.exam.*;
import lt.itakademija.exam.Package;

import java.util.*;

public class WarehouseImpl extends Warehouse {

    private List<Client> clients;
    private List<Package> packages;
    HashMap<Client, Package> storePackageList;

    public WarehouseImpl(IdGenerator clientIdGenerator, IdGenerator packageIdGenerator, int totalSpace) {
        super(clientIdGenerator, packageIdGenerator, totalSpace);
        this.clients = new LinkedList<>();
        this.packages = new LinkedList<>();
        this.storePackageList = new LinkedHashMap<Client, Package>();
    }

    @Override
    public int getTotalSpace() {
        return totalSpace;
    }

    @Override
    public int getAvailableSpace() {
//        int reservedSpace = 0;
//        for (Client c : clients) {
//            reservedSpace += c.getReservedSpace();
//        }
//        int availableSpace = totalSpace - reservedSpace;
        int availableSpace = totalSpace - getReservedSpace();
        return availableSpace;
    }

    @Override
    public int getTotalAvailableSpace() {
        int realReservedSpace = 0;
        for (Client c : clients) {
            realReservedSpace += c.getAvailableSpace();
        }
        int totalAvailableSpace = totalSpace - realReservedSpace;
        return totalAvailableSpace;
    }

    @Override
    public int getReservedSpace() {
        int reservedSpace = 0;

        for (Client c : clients) {
            reservedSpace += c.getReservedSpace();
        }
        return reservedSpace;
    }

    @Override
    public Client registerClient(String name, int reservingSpace) {

        Client client = new Client(clientIdGenerator.generateId(), name, reservingSpace);
        this.clients.add(client);
        return client;
    }

    @Override
    public Package createPackage(String name, int space) {
        return new Package(packageIdGenerator.generateId(), name, space);
    }

    @Override
    public void storePackage(Client client, Package aPackage) {
        client.addPackage(aPackage);
    }

    @Override
    public Client getClientById(int i) {
        for (Client cl : clients) {
            if (cl.getId() == i) {
                return cl;
            }
        }
        return null;
    }

    @Override
    public boolean hasClientById(int i) {
        for (Client cl : clients) {
            if (cl.getId() == i) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Client> findClientsBy(ClientPredicate clientPredicate) {
        List<Client> predicateClinetList = new LinkedList<>();
        for (Client c : clients) {
            if (clientPredicate.test(c) == true) {
                predicateClinetList.add(c);
            }
        }
        return predicateClinetList;
    }
}
